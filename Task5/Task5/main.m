//
//  main.m
//  Task5
//
//  Created by Андрей Александров on 4/8/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
