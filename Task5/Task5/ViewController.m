//
//  ViewController.m
//  Task5
//
//  Created by Андрей Александров on 4/9/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.nameField.delegate = self;
    self.nameField.tag = 1;
    
    self.nicknameField.delegate = self;
    self.nicknameField.tag = 2;
    
    self.emailField.delegate = self;
    self.emailField.tag = 3;
    
    self.phoneNumberField.delegate = self;
    self.phoneNumberField.tag = 4;
    
    self.passwordField.delegate = self;
    self.passwordField.tag = 5;
    
    self.confirmPasswordField.delegate = self;
    self.confirmPasswordField.tag = 6;
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapRecognizer];
    
    self.textViewField.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
}

- (void) dismissKeyboard {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    long nextTag = textField.tag + 1;
    [self goToNextField:textField withTag:nextTag];
    
    return NO;
}

- (void) goToNextField:(UITextField *)textField withTag:(long)nextTag {
    UIResponder *nextResponder = [self.view viewWithTag:nextTag];
    
    if([nextResponder isKindOfClass:[UITextField class]]) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
